/*
    Base_Object.hpp
    Purpose: - 

    @author Igor Sadza
    @version 0.1 - 14/03/19
*/
#ifndef CREATURE_OBJECT_HPP
#define CREATURE_OBJECT_HPP

#include "../utils/Common_Includes.hpp"
#include "Base_Object.hpp"
#include "../animation/Animation.hpp"
#include "../generators/Particle_Generator.hpp"

class CreatureObject;

struct SpawnPoint {
    private:
        GLfloat     m_duration;
        GLfloat     m_currentDuration;
        glm::vec2   m_position;
        std::vector<CreatureObject> m_registry;
    public:
        SpawnPoint(glm::vec2 t_position, GLfloat t_duration);
        GLvoid spawn();
        GLvoid render();
};

class CreatureObject : public BaseObject {
    private:

         GLvoid randomiseDirection();
         GLvoid checkPlayerPosition();

         glm::vec2 m_movementDestanation;
         
        Animation          *m_animation;
        ParticleGenerator  *m_particleGenerator;
        glm::vec2           m_position;
    public:
        CreatureObject(const std::string &t_pathImage, glm::vec2 t_startingPoint = glm::vec2(0.0f));
        GLvoid objectLogic();  


         GLvoid motion();     

}; // CreatureObject
#endif // CREATURE_OBJECT_HPP