/*
    Creature_Object.hpp
    Purpose: - 

    @author Igor Sadza
    @version 0.1 - 14/03/19
*/
#include "Creature_Object.hpp"
#include "../managers/Resource_Manager.hpp"
#include "../utils/Settings.hpp"
#include "Cursor_Object.hpp"
#include "../utils/Shared_Resources.hpp"

// ! CreatureObject class.

CreatureObject::CreatureObject(const std::string &t_pathImage,  glm::vec2 t_startingPoint) : BaseObject() {
    
    setImage(t_pathImage);
    getPrimitive().setPosition(t_startingPoint);

    setShader("resources/shaders/image.shader");

    m_animation = new Animation(t_pathImage, getImage());

    m_particleGenerator = new ParticleGenerator("resources/shaders/image.shader", 10);
    m_particleGenerator->setDrains(15.0f, 0.25f, 10.0f);
    m_particleGenerator->setImage(t_pathImage);
    m_particleGenerator->setScopeColor(0.0f, 0.0f, 1.0f);
    m_particleGenerator->setSpawnPointRange(getPrimitive().getPosition());

    randomiseDirection();
}

GLvoid CreatureObject::objectLogic() {
    motion();    
    m_animation->play();
    m_particleGenerator->render();
}

// ! Move struct.

GLvoid CreatureObject::motion() {

    if (m_movementDestanation.x > getPrimitive().getPosition().x) {
        getPrimitive().getPosition().x += 0.5f;
    } else {
       getPrimitive().getPosition().x -= 0.5f;
    }

    if (m_movementDestanation.y > getPrimitive().getPosition().y) {
        getPrimitive().getPosition().y += 0.5f;
    } else {
        getPrimitive().getPosition().y -= 0.5f;
    }

    
    if (m_movementDestanation == getPrimitive().getPosition()) {

        randomiseDirection();            
    }

    checkPlayerPosition();
}

GLvoid CreatureObject::randomiseDirection() {
    srand (time(NULL));
    GLuint rand_C = rand() % 100;
    GLfloat rand_x = rand() % GLuint(Settings::windowWidth) + rand_C;
    GLfloat rand_y = rand() % GLuint(Settings::windowHeight) + rand_C;
    m_movementDestanation = glm::vec2(rand_x, rand_y);
}

GLvoid CreatureObject::checkPlayerPosition() {
    glm::vec2 tmp = getPrimitive().getPosition();

    bool collisionX = tmp.x + 150.0f >= CursorObject::getCursorPosition().x &&
                      CursorObject::getCursorPosition().x + 25.0f >= tmp.x;

    bool collisionY =tmp.y + 150.0f >= CursorObject::getCursorPosition().y &&
                      CursorObject::getCursorPosition().y + 25.0f >=tmp.y;

    if(collisionX && collisionY) {
        m_movementDestanation = CursorObject::getCursorPosition();
    }
}

// ! SpawnPoint struct.

SpawnPoint::SpawnPoint(glm::vec2 t_position, GLfloat t_duration)
: m_duration(t_duration), m_position(t_position), m_currentDuration(0.0f) {

}

GLvoid SpawnPoint::spawn() {
    
    if((m_currentDuration += SharedResources::deltaTime) >= m_duration) {
            CreatureObject creature("resources/sprites/Enemy/Enemy_0/enemy_0.png", glm::vec2(200.0f));
            m_registry.push_back(creature);
            m_currentDuration = 0.0f;
        }
}

GLvoid SpawnPoint::render() {
  for (auto &ite : m_registry) {
    ite.objectLogic();
    ite.objectRender();
  }
}