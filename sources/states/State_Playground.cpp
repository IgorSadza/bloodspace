/*
    StatePlayground.cpp
    Purpose: State playground for testing.

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#include "State_Playground.hpp"

#include "../objects/Base_Object.hpp"
#include "../objects/Creature_Object.hpp"

static StatePlayground *statePlayground;


SpawnPoint *spawnPoint;

StatePlayground::StatePlayground() {
  statePlayground = this;
  BaseState::Register("StatePlayground", statePlayground);
  BaseState::setInstance("StatePlayground");

  spawnPoint = new SpawnPoint(glm::vec2(200.0f), 1.0f);
}

GLvoid StatePlayground::stateBackground() {

}

GLvoid StatePlayground::stateLogic() {
  spawnPoint->spawn();
  spawnPoint->render();
}

GLvoid StatePlayground::stateRender() { 

}