/*
    StateSwticher.cpp
    Purpose: State switcher.

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#include "State_Switcher.hpp"
#include "State_Playground.hpp"

#include "../renderers/Text_Renderer.hpp"
#include "../renderers/Primitive_Renderer.hpp"
#include "../objects/Cursor_Object.hpp"

StateSwitcher::StateSwitcher() {
    TextRenderer::initRenderer();
    PrimitiveRenderer::initRenderer();
    CursorObject::initCursor("resources/sprites/cursors/cursor_normal.png");

    //StateLogo *stateLogo = new StateLogo();    

    StatePlayground *statePlayground = new StatePlayground();
}

StateSwitcher &StateSwitcher::init() {
    static StateSwitcher stateSwitcher;
    return stateSwitcher;
}

GLvoid StateSwitcher::selectedStateLogic() {
    CursorObject::cursorLogic();
    BaseState::GetInstance()->stateLogic();
}

GLvoid StateSwitcher::selectedStateRender() {
    CursorObject::cursorRender();
    BaseState::GetInstance()->stateRender();
}