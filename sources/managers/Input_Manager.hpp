/*
    InputManager.hpp
    Purpose: Input manager.

    @author Igor Sadza
    @version 0.1 - 08/03/19
*/
#ifndef INPUT_MANAGER_HPP
#define INPUT_MANAGER_HPP

#include "../utils/Common_Includes.hpp"

class InputManager {
    private:
        struct Setter {
            public:
                static GLvoid keyMouse(GLuint t_key, GLint t_action);
                static GLvoid keyKeyboard(GLuint t_key, GLint t_action);
                static GLvoid positionMouse(glm::vec2 t_position);
                static GLvoid positionMouse(GLfloat t_x, GLfloat t_y);
        }; // Setter

        struct Getter {
            public:
                static GLboolean keyMouse(GLuint t_key);
                static GLboolean keyKeyboard(GLuint t_key);
                static glm::vec2 positionMouse();
        }; // Getter

        InputManager();
        static GLboolean    *m_mouseKeys;
        static GLboolean    *m_keyboardKeys;
        static glm::vec2    m_mousePosition;

        static Setter *m_setter;
        static Getter *m_getter;

    public:
        static InputManager &init();

        static Setter set();
        static Getter get();
        
}; // InputManager

#endif // INPUT_MANAGER_HPP
