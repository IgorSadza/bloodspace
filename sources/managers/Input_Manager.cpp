/*
    InputManager.cpp
    Purpose: Input manager.

    @author Igor Sadza
    @version 0.1 - 08/03/19
*/
#include "Input_Manager.hpp"

// ! Input_Manager class.

GLboolean *InputManager::m_mouseKeys;
GLboolean *InputManager::m_keyboardKeys;
glm::vec2 InputManager::m_mousePosition;

InputManager::InputManager() {
    m_mouseKeys = new GLboolean[15];
    m_keyboardKeys = new GLboolean[260];
}

InputManager &InputManager::init() {
    static InputManager inputManager;
    return inputManager;
}

InputManager::Setter InputManager::set() {
    return Setter();
}

InputManager::Getter InputManager::get() {
    return Getter();
}

// ! Setter class.

GLvoid InputManager::Setter::keyMouse(GLuint t_key, GLint t_action) {
    InputManager::m_mouseKeys[t_key] = t_action;
}
GLvoid InputManager::Setter::keyKeyboard(GLuint t_key, GLint t_action) {
    InputManager::m_keyboardKeys[t_key] = t_action;
}
GLvoid InputManager::Setter::positionMouse(glm::vec2 t_position) {
    InputManager::m_mousePosition = t_position;
}
GLvoid InputManager::Setter::positionMouse(GLfloat t_x, GLfloat t_y) {
    InputManager::m_mousePosition = glm::vec2(t_x, t_y);
}


// ! Getter class.

GLboolean InputManager::Getter::keyMouse(GLuint t_key) {
    return InputManager::m_mouseKeys[t_key];
}
GLboolean InputManager::Getter::keyKeyboard(GLuint t_key) {
    return InputManager::m_keyboardKeys[t_key];
}
glm::vec2 InputManager::Getter::positionMouse() {
    return InputManager::m_mousePosition;
}